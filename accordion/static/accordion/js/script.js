$(".accordion-title").map(function(index, item) {
    item.onclick = function(event) {
        if ($(this).parent().next().hasClass("hide")) {

            let opened = undefined;

            $(".accordion-content").map(function(index, item) {
                if (!$(this).hasClass("hide")) {
                    opened = $(this);
                }
            })

            console.log(opened);
    
            $(this).parent().next().slideDown(300);
            $(this).parent().next().removeClass("hide");

            opened?.slideUp(300)
            opened?.addClass("hide")
        } else {
            $(this).parent().next().slideUp(300);
            $(this).parent().next().addClass("hide");
        }
    }
})

$(".button-up").map((index, button) => (
    button.onclick = function() {
        const currentItem = $(this).parent().parent().parent();
        currentItem.insertBefore(currentItem.prev())
    }
))

$(".button-down").map((index, button) => (
    button.onclick = function() {
        const currentItem = $(this).parent().parent().parent();
        currentItem.insertAfter(currentItem.next())
    }
))
