from django import forms
from .models import Formulir, Peserta

class FormulirForm(forms.ModelForm):
    class Meta:
        model = Formulir

        fields = [
            'Kegiatan',
            'Hari',
            'Tanggal',
            'Jam',
            'Tempat',
            'Kategori',
        ]

        widgets = {
            'Kegiatan': forms.TextInput(
                attrs = {
                    'class' : 'form-control',
                    'place-holder' : 'Kegiatan'
                }
            ),

            'Hari' : forms.Select(
                attrs = {
                    'class' : 'form-control'
                }
            ),

            'Tanggal': forms.TextInput(
                attrs = {
                    'class' : 'form-control form-coloumn',
                    'type' : 'date'
                }
            ),

            'Jam' : forms.TextInput(
                attrs = {
                    'class' : 'form-control',
                    'type' : 'time'
                }   
            ),

            'Tempat' : forms.TextInput(
                attrs = {
                    'class' : 'form-control',
                    'place-holder' : 'ex : Warnet'
                }
            ),

            'Kategori' : forms.TextInput(
                attrs = {
                'class' : 'form-control',
                'place-holder' : 'ex : Belajar'
                }
            ),   
        }

class PesertaForm(forms.ModelForm):
    class Meta:
        model = Peserta
        fields = [
            'Nama',
            'Kegiatan',
        ]

        widgets = {
            'Nama': forms.TextInput(
                attrs = {
                    'class': 'form-control',
                    'placeholder': 'Tambahkan peserta'
                }
            )
        }

