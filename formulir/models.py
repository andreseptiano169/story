from django.db import models

# Create your models here.

class Formulir(models.Model):
    list_hari = (
        ('Senin', 'Senin'),
        ('Selasa', 'Selasa'),
        ('Rabu', 'Rabu'),
        ('Kamis', 'Kamis'),
        ('Jum\'at', 'Jum\'at'),
        ('Sabtu', 'Sabtu'),
        ('Minggu', 'Minggu'),
    )

    Kegiatan    = models.CharField(max_length=50)
    Hari        = models.CharField(max_length=50, choices=list_hari, default='Senin')
    Tanggal     = models.DateField(max_length=50)
    Jam         = models.TimeField(max_length=50)
    Tempat      = models.CharField(max_length=50)
    Kategori    = models.CharField(max_length=50)

    def __str__(self):
        return "{}.{}".format(self.id, self.Kegiatan)


class Peserta(models.Model):
    Nama        = models.CharField(max_length=50)
    Kegiatan    = models.ForeignKey(Formulir, on_delete = models.CASCADE)

    def __str__(self):
        return self.Nama