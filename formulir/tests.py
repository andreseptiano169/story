import unittest
from django.test import TestCase, Client
from django.urls import resolve
from .views import index, create, delete, addpeserta, deletepeserta
from .models import Formulir, Peserta

class FormulirTest(TestCase):
    #URL testcase
    def test_url_is_exist_kegiatan(self):
        response = Client().get('/kegiatan/')
        self.assertEqual(response.status_code, 200)

    #Views testcase
    def test_template_kegiatan_using_index_func(self):
        found = resolve('/kegiatan/')
        self.assertEqual(found.func, index)

    #Models testcase
    def test_model_can_create_new_kegiatan(self):
        #Creating a new activity
        kegiatan_baru = Formulir.objects.create(
            Kegiatan='Grinding Genshin',
            Hari='Rabu',
            Tanggal='2021-04-14',
            Jam='7:30 a.m.',
            Tempat='Rumah',
            Kategori='Ngabuburit'
            )
        #Retrieving all available activity
        counting_all_available_activity = Formulir.objects.all().count()
        self.assertEqual(counting_all_available_activity,1)

    #Models testcase
    def test_models_str_return_is_correct(self):
        kegiatan_baru = Formulir.objects.create(
            Kegiatan='Grinding Genshin',
            Hari='Rabu',
            Tanggal='2021-04-14',
            Jam='7:30 a.m.',
            Tempat='Rumah',
            Kategori='Ngabuburit'
            )
        self.assertEqual("{}.{}".format(kegiatan_baru.id, kegiatan_baru.Kegiatan), "{}.{}".format(kegiatan_baru.id, 'Grinding Genshin'))

    #URL testcase
    def test_url_is_exist_kegiatan_create(self):
        response = Client().get('/kegiatan/create/')
        self.assertEqual(response.status_code, 200)

    #Views testcase
    def test_view_kegiatan_create_is_using_function_create(self):
        found = resolve('/kegiatan/create/')
        self.assertEqual(found.func, create)

    #HTML testcase
    def test_template_kegiatan_using_index_template(self):
        response = Client().get('/kegiatan/')
        self.assertTemplateUsed(response, 'kegiatan/index.html')
    
    #Models testcase
    def test_model_can_create_new_peserta(self):
        kegiatan_baru = Formulir.objects.create(
            Kegiatan='Grinding Genshin',
            Hari='Rabu',
            Tanggal='2021-04-14',
            Jam='7:30 a.m.',
            Tempat='Rumah',
            Kategori='Ngabuburit'
        )

        peserta_baru = Peserta.objects.create(Nama='Andre', Kegiatan=kegiatan_baru)

        count_all_available_peserta = Peserta.objects.all().count()
        self.assertEqual(count_all_available_peserta, 1)