from django.urls import path

from . import views

app_name = 'formulir'

urlpatterns = [
    path('', views.index, name='index'),
    path('create/', views.create, name='create'),
    path('delete/<int:delete_id>/', views.delete, name='delete'),
]
