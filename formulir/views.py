from django.shortcuts import render, redirect
from .models import Formulir, Peserta
from .forms import FormulirForm, PesertaForm


# Create your views here.
def index(request):
    semua_kegiatan = Formulir.objects.all()
    form_peserta = PesertaForm()
    list_peserta = [[i, Peserta.objects.filter(Kegiatan=i)] for i in semua_kegiatan]

    context = {
        'semua_kegiatan' : semua_kegiatan,
        'form_peserta' : form_peserta,
        'list_peserta' : list_peserta,
    }

    return render(request, 'kegiatan/index.html', context)

def create(request):
    kegiatan_form = FormulirForm(request.POST or None)
    if request.method == 'POST':
        if kegiatan_form.is_valid():
            kegiatan_form.save()
            
            return redirect('formulir:index')
    
    context = {
        'kegiatan_form' : kegiatan_form,
    }

    return render(request, 'kegiatan/create.html', context)

def delete(request, delete_id):
    Formulir.objects.filter(id=delete_id).delete()
    return redirect('formulir:index')


def addpeserta(request):
    form = PesertaForm(request.POST or None)
    if (request.method == 'POST'):
        if (form.is_valid):
            form.save()
    return redirect('formulir:index')

def deletepeserta(request, delete_id):
    Peserta.objects.get(id=delete_id).delete()
    return redirect('formulir:index')

