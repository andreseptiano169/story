from django import forms
from .models import Kuliah

class KuliahForm(forms.ModelForm):
    class Meta:
        model = Kuliah
        fields = [
            'Mata_Kuliah',
            'Dosen_Pengajar',
            'Jumlah_SKS',
            'Deskripsi',
            'Ruang_Kelas',
            'Semester',
        ]
