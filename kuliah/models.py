from django.db import models
from .validator import sks_validator, semester_validator

# Create your models here.


class Kuliah(models.Model):
    Mata_Kuliah       = models.CharField(max_length = 100)
    Dosen_Pengajar    = models.CharField(max_length = 100)
    Jumlah_SKS        = models.CharField(
        max_length = 1,
        validators = [sks_validator]
        )
    Deskripsi         = models.TextField()
    Ruang_Kelas       = models.CharField(max_length = 100)
    Semester          = models.CharField(
        max_length = 1,
        validators = [semester_validator]
        )

    def __str__(self):
        return "{}.{}".format(self.id, self.Mata_Kuliah)
