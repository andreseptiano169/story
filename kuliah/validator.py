from django.core.exceptions import ValidationError

def sks_validator(value):
    jumlah_sks = value
    valid = jumlah_sks.isdigit() and int(jumlah_sks) > 0

    if(not valid):
        message = "Masukkan Jumlah SKS dengan Benar!"
        raise ValidationError(message)

def semester_validator(value):
    semester = value
    valid = semester.isdigit() and int(semester) > 0 and int(semester) < 9

    if(not valid):
        message = "Masukkan Semester Dengan Benar!"
        raise ValidationError(message)