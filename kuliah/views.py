from django.shortcuts import render, redirect

from .forms import KuliahForm
from .models import Kuliah


def index(request):
    semua_kuliah = Kuliah.objects.all()

    context = {
        'semua_kuliah':semua_kuliah,
    }

    return render(request, 'html/index.html', context)

def create(request):
    kuliah_form = KuliahForm(request.POST or None)
    if request.method == 'POST':
        if kuliah_form.is_valid():
            kuliah_form.save()
            
            return redirect('kuliah:index')
    
    context = {
        'kuliah_form':kuliah_form,
    }

    return render(request, 'html/create.html', context)

def delete(request, delete_id):
    Kuliah.objects.filter(id=delete_id).delete()
    return redirect('kuliah:index')

def detail(request, detail_id):
    kuliah_detail = Kuliah.objects.get(id=detail_id)
    context = {
        'kuliah_detail':kuliah_detail,
    }
    
    return render(request, 'html/detail.html', context)
