let count = 10;
$("#query-result").hide();
$("#search").keyup(async function() {
    const query = $(this).val();
    count = 10;
    if (query.length >= 3){
        const result = await $.ajax({
            url: "/story8/api/books/" + query + "/" + count + "/",
        })
        $("#query-result").show();
        $("#books-data").empty();
        $("#books-data").append("<p>Menampilkan hasil pencarian " + query + "</p>");
        if (result){
            items = result.items.map((item) => getHTMLElements(item.volumeInfo));
            for (const item in items){
                $("#books-data").append(items[item])
            }
        }
    } else {
        $("#books-data").empty();
    }
});

$(window).scroll(async function() {
    let position = $(window).scrollTop();
    let bottom = $(document).height() - $(window).height();

    if (position === bottom){
        count += 10;
        if(count > 40){
            count = 40;
        }
        const query = $("#search").val();
        if (query.length >= 3){
            const result = await $.ajax({
                url: "/story8/api/books/" + query + "/" + count + "/",
            })
            $("#query-result").show();
            $("#books-data").empty();
            $("#books-data").append("<p>Menampilkan hasil pencarian " + query + "</p>");
            if (result){
                items = result.items.map((item) => getHTMLElements(item.volumeInfo));
                for (const item in items){
                    $("#books-data").append(items[item])
                }
            }
        } else {
            $("#books-data").empty();
        }
    }
});

function getHTMLElements(data){
    return `
        <div class="box d-flex align-items-start p-2 border-top border-bottom">    
            <img src=${data.imageLinks?.thumbnail} />
            <div style="margin-left: 10px">
                <p>${data.title}</p>
                <p>${data.description || "No Description"}</p>
            </div>
        </div>
    `
}