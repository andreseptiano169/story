from django.test import TestCase

# Create your tests here.
from django.test import TestCase, Client
from django.urls import resolve

from .views import index, api_books

# Create your tests here.
class Story7Test(TestCase):

    # URL Testcases
    def test_url_is_exist_story8(self):
        response = Client().get('/story8/')
        self.assertEqual(response.status_code, 200)

    def test_url_is_exist_api_books(self):
        response = Client().get('/story8/api/books/harry/10/')
        self.assertEqual(response.status_code, 200)

    # View Testcases
    def test_view_story8_is_using_function_index(self):
        found = resolve('/story8/')
        self.assertEqual(found.func, index)

    def test_view_api_books_is_using_function_api_books(self):
        found = resolve('/story8/api/books/harry/10/')
        self.assertEqual(found.func, api_books)

    # Template Testcases
    def test_template_story8_using_index_template(self):
        response = Client().get('/story8/')
        self.assertTemplateUsed(response, 'story8/story8.html')
