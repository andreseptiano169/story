from django.urls import path
from .views import index, api_books

app_name = 'story8'

urlpatterns = [
	path('', index, name = 'index'),
    path('api/books/<str:query>/<int:count>/', api_books, name = 'api_books'),
]