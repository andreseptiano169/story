from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from django.http import HttpResponse
import json
import requests

# Create your views here.
def index(request):
    return render(request, 'story8/story8.html')

def api_books(request, query, count):
    req = requests.get("https://www.googleapis.com/books/v1/volumes?q=" + query + "&maxResults=" + str(count))
    data = req.json()
    data = json.dumps(data)
    return HttpResponse(content = data, content_type="application/json")
