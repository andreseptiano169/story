from django.test import TestCase, Client
from django.urls import resolve
from django.contrib.auth.models import User

from .views import userlogin, userregister

# Create your tests here.
class UserAuthTest(TestCase):
    '''
        URL Testcases
    '''
    def test_url_is_exist_login(self):
        response = Client().get('/userauth/login/')
        self.assertEqual(response.status_code, 200)

    def test_url_is_exist_register(self):
        response = Client().get('/userauth/register/')
        self.assertEqual(response.status_code, 200)
    
    def test_url_is_exist_logout(self):
        response = Client().get('/userauth/logout/')
        self.assertEqual(response.status_code, 302)

    '''
        Views Testcases
    '''
    def test_view_cannot_access_login_if_user_already_authenticated(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.set_password('password_for_dummy')
        new_account.save()

        c = Client()
        login = c.login(username = new_account.username, password = 'password_for_dummy')
        response = c.get('/userauth/login/')
        self.assertEqual(response.status_code, 302)

    def test_view_cannot_access_register_if_user_already_authenticated(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.set_password('password_for_dummy')
        new_account.save()

        c = Client()
        login = c.login(username = new_account.username, password = 'password_for_dummy')
        response = c.get('/userauth/register/')
        self.assertEqual(response.status_code, 302)

    def test_view_can_login_from_post_request(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.set_password('password_for_dummy')
        new_account.save()

        c = Client()
        response = c.post(
            '/userauth/login/',
            data = {
                'nama': 'dummy',
                'password': 'password_for_dummy'
            }
        )

        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')

    def test_view_can_register_from_post_request(self):
        c = Client()
        response = c.post(
            '/userauth/register/',
            data = {
                'nama': 'dummy',
                'password': 'password_for_dummy'
            }
        )

        login = c.login(username = 'dummy', password = 'password_for_dummy')
        response = c.get('/userauth/login/')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(response['location'], '/')
        response = c.get('/userauth/register/')
        self.assertEqual(response.status_code, 302)

    def test_view_login_failed(self):
        c = Client()
        response = c.post(
            '/userauth/login/',
            data = {
                'nama': 'dummy',
                'password': 'password_for_dummy'
            }
        )

        self.assertEqual(response.status_code, 200)
        html_response = response.content.decode('utf8')
        self.assertIn('Autentikasi gagal', html_response)
    
    def test_register_failed_exists_account(self):
        new_account = User.objects.create_user('dummy', '', 'password_for_dummy')
        new_account.set_password('password_for_dummy')
        new_account.save()

        c = Client()
        response = c.post(
            '/userauth/register/',
            data = {
                'nama': 'dummy',
                'password': 'password_for_dummy'
            }
        )

        self.assertEqual(response.status_code, 200)
        html_response = response.content.decode('utf8')
        self.assertIn('Sudah ada akun dengan username dummy', html_response)


