from django.urls import path
from .views import userlogin, userregister, userlogout

app_name = 'userauth'

urlpatterns = [
	path('login/', userlogin, name = 'login'),
	path('register/', userregister, name = 'register'),
    path('logout/', userlogout, name = 'logout'),
]
