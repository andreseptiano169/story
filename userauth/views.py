from django.shortcuts import render, redirect, resolve_url
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User 

from .forms import AuthForm

# Create your views here.
def userlogin(request):
    if (not request.user.is_anonymous):
	    return redirect(resolve_url('main:home'))

    form = AuthForm(request.POST or None)
    context = {
        'Form': form,
        'isError': False,
        'Error': '',
    }

    if (request.method == "POST"):
        account = authenticate(
            username = request.POST['nama'],
            password = request.POST['password'],
        )

        if (account == None):
            context['isError'] = True
            context['Error'] = 'Autentikasi gagal'
            context['Form'] = AuthForm()
            return render(request, 'userauth/login.html', context)
        else:
            login(request, account)
            return redirect(resolve_url('main:home'))

    return render(request, 'userauth/login.html', context)

def userregister(request):
    if (not request.user.is_anonymous):
	    return redirect(resolve_url('main:home'))

    form = AuthForm(request.POST or None)
    context = {
        'Form': form,
        'isError': False,
        'Error': '',
    }

    if (request.method == "POST"):
        account = None
        try:
            account = User.objects.get(username = request.POST['nama'])
        except:
            account = None
        if (account == None):
            new_account = User.objects.create_user(request.POST['nama'], '', request.POST['password'])
            new_account.save()
            login(request, new_account)
            return redirect(resolve_url('main:home'))
        else:
            context['isError'] = True
            context['Error'] = 'Sudah ada akun dengan username ' + request.POST['nama']
            context['Form'] = AuthForm()
            return render(request, 'userauth/register.html', context)

    return render(request, 'userauth/register.html', context)

def userlogout(request):
    logout(request)
    return redirect(resolve_url('main:home'))
